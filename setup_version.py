# -*- coding: utf-8 -*-
# Copyright (c) 2015, PyRETIS Development Team.
# Distributed under the LGPLv2.1+ License. See LICENSE for more info.
"""
PyRETIS - A simulation package for rare event simulations.
Copyright (C) 2015  The PyRETIS team

This file only generates the verison info.
"""
import os
import subprocess


# For setting version. This is copied from Numpy's setup.py.
MAJOR = 1
MINOR = 0
MICRO = 0
DEV = 4
ISRELEASED = False
if not ISRELEASED:
    VERSION = '{:d}.{:d}.{:d}.dev{:d}'.format(MAJOR, MINOR, MICRO, DEV)
else:
    VERSION = '{:d}.{:d}.{:d}'.format(MAJOR, MINOR, MICRO)
VERSION_FILE = os.path.join('pyretis', 'version.py')
VERSION_TXT = '''# -*- coding: utf-8 -*-
# Copyright (c) 2015, PyRETIS Development Team.
# Distributed under the LGPLv2.1+ License. See LICENSE for more info.
"""Version information for PyRETIS.

This file is generated by PyRETIS (``setup_version.py``)
"""
SHORT_VERSION = '{0:s}'
VERSION = '{0:s}'
FULL_VERSION = '{1:s}'
GIT_REVISION = '{2:s}'
GIT_VERSION = '{3:s}'
RELEASE = {4:}

if not RELEASE:
    VERSION = GIT_VERSION
'''


SETUP_PY = 'setup.py'


def get_git_version():
    """Method to obtain the git revision as a string.

    This method is adapted from Numpy's setup.py

    Returns
    -------
    git_revision : string
        The git revision, it the git revision could not be determined,
        a 'Unknown' will be returned.
    """
    git_revision = 'Unknown'
    try:
        env = {}
        for key in ('SYSTEMROOT', 'PATH'):
            val = os.environ.get(key)
            if val is not None:
                env[key] = val
        # LANGUAGE is used on win32
        env['LANGUAGE'] = 'C'
        env['LANG'] = 'C'
        env['LC_ALL'] = 'C'
        out = subprocess.Popen(['git', 'rev-parse', 'HEAD'],
                               stdout=subprocess.PIPE,
                               env=env).communicate()[0]
        git_revision = out.strip().decode('ascii')
    except OSError:
        git_revision = 'Unknown'
    return git_revision


def get_version_info():
    """Return the version number for PyRETIS.

    This method is adapted from Numpy's setup.py.

    Returns
    -------
    full_version : string
        The full version string for this release.
    git_revision : string
        The git revision number.
    """
    if os.path.exists('.git'):
        git_revision = get_git_version()
    elif os.path.exists(VERSION_FILE):
        try:
            from pyretis.version import git_revision
        except ImportError:
            raise ImportError('Unable to import git_revision. Try removing '
                              'pyretis/version.py and the build directory '
                              'before building.')
    else:
        git_revision = 'Unknown'
    if not ISRELEASED:
        git_version = ''.join([VERSION.split('dev')[0],
                               'dev{:d}+'.format(DEV),
                               git_revision[:7]])
    else:
        git_version = VERSION
    full_version = VERSION
    return full_version, git_revision, git_version


def write_version_py():
    """Create a file with the version info for PyRETIS.

    This method is adapted from Numpy's setup.py.
    """
    full_version, git_revision, git_version = get_version_info()
    version_txt = VERSION_TXT.format(VERSION, full_version,
                                     git_revision, git_version, ISRELEASED)
    with open(VERSION_FILE, 'wt') as vfile:
        try:  # will work in python 3
            vfile.write(version_txt)
        except UnicodeEncodeError:  # for python 2
            vfile.write(version_txt.encode('utf-8'))
    return full_version


def write_version_in_setup_py(version):
    """Update version for setup.py as well.

    setup.py is a small file so we just read it into memory here."""
    tmp = []
    with open(SETUP_PY, 'r') as sfile:
        for lines in sfile:
            if lines.startswith('FULL_VERSION ='):
                tmp.append(("FULL_VERSION = '{}'\n".format(version)))
            else:
                tmp.append(lines)
    with open(SETUP_PY, 'wt') as sfile:
        for lines in tmp:
            sfile.write(lines)


if __name__ == '__main__':
    FULL_VERSION = write_version_py()
    print('Setting version to: {}'.format(FULL_VERSION))
    write_version_in_setup_py(FULL_VERSION)
