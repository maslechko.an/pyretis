# -*- coding: utf-8 -*-
# Copyright (c) 2015, PyRETIS Development Team.
# Distributed under the LGPLv2.1+ License. See LICENSE for more info.
"""This module just contains some info for PyRETIS.

Here we define the name of the program and some other
relevant info.
"""
PROGRAM_NAME = 'PyRETIS'
URL = 'http://www.pyretis.org'
GIT_URL = 'https://gitlab.com/pyretis/pyretis'
CITE = """
[1] A. Lervik, E. Riccardi and T. S. van Erp, J. Comput. Chem., 2017
    doi: http://dx.doi.org/10.1002/jcc.24900
"""
