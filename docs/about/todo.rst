.. _todo-notes:

####
Todo
####

|pyretis| is still in development. Please visit our git repository
at `<https://gitlab.com/pyretis/pyretis>`_ for information about
the development process. Below is a list of work that remains
to do (or could be considered):

- Finish input file format.

- Implementation of new rare event methods. Improved shooting etc.

- Interface MD packages like LAMMPS, NAMD, etc.

- Include option for storing/loading binary files.

- The reports could be nicer.
