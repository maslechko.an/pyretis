.. _release-notes:

#############
Release Notes
#############

The **current** |pyretis| version is: |version|


Upgrading
=========

To upgrade |pyretis| to the latest version, use ``pip``:

.. code-block:: pyretis

    pip install -U pyretis

You can determine your currently installed version using ``pip freeze``:

.. code-block:: pyretis

    pip freeze | grep pyretis

Version history
===============

0.1.0 (2016.01.01)
------------------

-  Non-public release
-  This release was mainly used for testing simple 1D and 2D potentials.

0.2.0 (2016.10.01)
------------------

- First semi-public release. This release was used for demonstrating the
  RETIS algorithm in a hands-on session at the CECAM School
  "Multiscale Simulations of Soft Matter with Hands-On
  Tutorials on ESPResSo++ and VOTCA", Schloss Waldthausen in Mainz,
  October 10 to 13 2016.

0.6.0 (2017.03.22)
------------------

- Added restart capabilities.
- Added capabilities for using GROMACS.

0.9.0 (2017.04.10)
------------------

- Improved restart capabilities.
- Added CP2K interface.

1.0.0 (2017.08.18)
------------------

- First real public release.
