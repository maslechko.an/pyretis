.. _about-index:

###############
About |pyretis|
###############

|pyretis| is a `Python <https://www.python.org>`_ library
for **rare event molecular simulations**
with emphasis on methods based on
:ref:`transition interface sampling <user-guide-tis-theory>`
and :ref:`replica exchange transition interface sampling <user-guide-retis-theory>`.
The paper describing the |pyretis| program
can be found here: `<https://doi.org/10.1002/jcc.24900>`_

The work on |pyretis| was initiated by `Titus van Erp <http://www.van-erp.org>`_ and is used in
the research activities in the
`applied theoretical chemistry <http://www.ntnu.edu/chemistry/research/theoretical-chemistry>`_
group at the `Norwegian University of Science and Technology <http://www.ntnu.edu/>`_.

|pyretis| is open source and is released
under a :ref:`GNU Lesser General Public license v3 <pyretis-license>`.
It is still under active development and a
list of :ref:`planned work <todo-notes>`
and :ref:`version changes <release-notes>` are available.
If you are interested in contributing to the |pyretis| project,
please have a look
to the :ref:`developer guide <developer-guide-index>` and visit our
git repository `<https://gitlab.com/pyretis/pyretis>`_.

.. toctree::
    :maxdepth: 2

    license.rst
    release-notes.rst
    todo.rst
