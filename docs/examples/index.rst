.. _examples-index:

########
Examples
########

We have prepared several examples to show the use of |pyretis|.
All examples are inlcuded in the source code distribution.

Complete list of examples:

.. toctree::
    :maxdepth: 2

    examples-umbrella.rst
    examples-md.rst
    examples-tis-1d.rst
    examples-retis-1d.rst
    examples-2d-hysteresis.rst
    examples-vvexternal.rst
    examples-md-fb.rst
    examples-pso.rst
    examples-gromacs-hydrate.rst
    examples-retis-wca.rst
    examples-cp2k-hydrogen.rst
