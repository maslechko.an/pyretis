
.. _api-initiation:

pyretis.initiation package
==========================

.. automodule:: pyretis.initiation
    :members:
    :undoc-members:
    :show-inheritance:

List of submodules
------------------

* :ref:`pyretis.initiation.initiate_kick <api-initiation-kick>`
* :ref:`pyretis.initiation.initiate_load <api-initiation-load>`

.. _api-initiation-kick:

pyretis.initiation.initiate_kick module
---------------------------------------

.. automodule:: pyretis.initiation.initiate_kick
    :members:
    :undoc-members:
    :show-inheritance:

.. _api-initiation-load:

pyretis.initiation.initiate_load module
---------------------------------------

.. automodule:: pyretis.initiation.initiate_load
    :members:
    :undoc-members:
    :show-inheritance:
