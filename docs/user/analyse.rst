.. _user-guide-analyse:

The |pyretis| analysis application
==================================

The |pyretis| analysis application, ``pyretisanalyse``, is used to
analyse the results from |pyretis| simulations.
The general syntax for executing is:

.. code-block:: pyretis

   pyretisanalyse [-h] -i INPUT [-V]


Input arguments
---------------

.. _tableappargument_analyse:

.. table:: Description of input arguments for pyretisanalyse

   +-------------------------------------+--------------------------------------------------+
   | Argument                            | Description                                      |
   +=====================================+==================================================+
   | -h, --help                          | Show the help message and exit                   |
   +-------------------------------------+--------------------------------------------------+
   | -i INPUT, --input INPUT             | Location of the input file.                      |
   +-------------------------------------+--------------------------------------------------+
   | -V, --version                       | Show the version number and exit.                |
   +-------------------------------------+--------------------------------------------------+
