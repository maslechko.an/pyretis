
.. image:: https://gitlab.com/pyretis/pyretis/badges/master/pipeline.svg
    :target: https://gitlab.com/pyretis/pyretis/commits/master

.. image:: https://gitlab.com/pyretis/pyretis/badges/master/coverage.svg
    :target: https://gitlab.com/pyretis/pyretis/commits/master


PyRETIS
=======

PyRETIS is a library for **rare event simulations**
with emphasis on methods based on transition interface sampling
replica exchange transition interface sampling.

PyRETIS is open source (see the COPYING and COPYING.LESSER files)
and can be interfaced with other simulation packages such as GROMACS or
CP2K.

The documentation is located at http://www.pyretis.org or it can
found in the docs directory of the source code.
