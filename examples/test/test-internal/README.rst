Test internal
=============

This folder contains some tests for using PyRETIS with the
internal engines. The different tests are described below.

compare-internal-with-lammps
----------------------------
This test will check that the internal engines give comparable
results to independent simulations performed with LAMMPS.

md-restart
----------
This test will check the restart capabilities of internal
engines when performing MD simulations.

retis-restart
-------------
This test will check the restart capabilities of internal
engines when performing a RETIS simulation.
