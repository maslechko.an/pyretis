Molecular dynamics example settings
===================================

Simulation settings
-------------------
task = md-nve
steps = 1000
restart = ../run-100/pyretis.restart

Engine settings
---------------
class = Langevin
timestep = 0.002
gamma = 0.3
seed = 0
high_friction = False

System settings
---------------
units = lj

Forcefield settings
--------------------
description = Lennard Jones test

Potential
---------
class = PairLennardJonesCutnp
shift = True
parameter 0 = {'sigma': 1.0, 'epsilon': 1.0, 'rcut': 2.5}

Output
------
prefix = md-100-1000-
backup = overwrite
energy-file = 1
order-file = 10
cross-file = 1
trajectory-file = 10
restart-file = 10
