test-cp2k
=========

Here, we test come core functionality when using CP2K as an
external engine. The different tests as described below.

test-cp2k
---------
This test that we can use CP2K for running dynamics, generating velocities
and so on.
