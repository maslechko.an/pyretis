Test GROMACS
============

This folder contains some tests for using GROMACS as an
external integrator. The different tests are described below.

test-gromacs
------------
This test will check that the core functionality of the
GROMACS engines are in place and that they give comparable results
to running just plain MD simulations with GROMACS.

test-gromacs-gromacs2
---------------------
This test will compare the two engines ``GROMACS``
and ``GROMACS2`` by running two short RETIS simulations.
