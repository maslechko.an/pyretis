C extension for the 2D WCA example
==================================

This folder contains a implementation of the
2D WCA example (the potential and order parameter for the
high barrier case) in C.

The C code must be compiled before it can be executed and this
is done by running ``python setup.py build_ext --inplace``.

Note: This implementation will **only** work in python3.
