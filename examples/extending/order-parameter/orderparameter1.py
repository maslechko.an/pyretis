# -*- coding: utf-8 -*-
# Copyright (c) 2015, pyretis Development Team.
# Distributed under the LGPLv2.1+ License. See LICENSE for more info.
"""This file contains examples for order parameters.

This file is distributed as part of the documentation of pyretis.
"""
import numpy as np
from pyretis.orderparameter import OrderParameter


class PlaneDistanceX(OrderParameter):
    """A positional order parameter.

    This class defines a very simple order parameter which is just
    the position of a given particle.
    """

    def __init__(self, index, plane_position):
        """Initialize the order parameter.

        Parameters
        ----------
        index : integer
            Selects the particle to use.

        plane_position : float
            The location of the plane, along the x-axis.

        """
        txt = 'Distance from particle {} to the plane at {}'.format(
            index,
            plane_position)
        super().__init__('Plane distance, x-axis', desc=txt)
        print(index, plane_position)
        self.index = index
        self.plane_position = plane_position

    def calculate(self, system):
        """Calculate the order parameter."""
        pos = system.particles.pos[self.index]
        dist = -np.abs(pos[0] - self.plane_position)
        print(dist)
        return dist

    def calculate_velocity(self, system):
        """Calculate the time derivative of the order parameter."""
        pass
