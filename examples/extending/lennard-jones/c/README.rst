Extending PyRETIS with C
========================

This folder contains an example of extending PyRETIS with a new
force field which is implemented in C.

The C code must be compiled before it can be executed and this
is done by running ``python3 setup.py build_ext --inplace``.

This particular example is for python3.
