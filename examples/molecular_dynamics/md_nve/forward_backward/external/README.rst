md_forward_backward_ext.py
==========================

This example will run a specified steps with the Velocity Verlet integrator
and then reverse the velocities and run for the same number of steps.

In this example c or fortran is used to speed up the simulation.
