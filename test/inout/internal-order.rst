Orderparameter
--------------
class = OrderParameter
name =  test

Collective-variable
-------------------
class = OrderParameterPosition
name = Position
index = 0
dim = x
periodic = False

Collective-variable
-------------------
class = OrderParameterDistance
name = My distance
index = (100, 101)
periodic = False
