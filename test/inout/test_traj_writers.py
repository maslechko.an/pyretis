# -*- coding: utf-8 -*-
# Copyright (c) 2015, PyRETIS Development Team.
# Distributed under the LGPLv2.1+ License. See LICENSE for more info.
"""Test the trajectory writers."""
import logging
import unittest
import tempfile
import os
import numpy as np
from numpy.random import rand, random, randint
from pyretis.core import create_box, System, Particles, Path, PathExt
from pyretis.tools.lattice import generate_lattice
from pyretis.inout.writers.writers import (
    adjust_coordinate,
    TrajWriter,
    PathIntWriter,
    PathExtWriter,
)
from pyretis.inout.writers import get_writer
logging.disable(logging.CRITICAL)


HERE = os.path.abspath(os.path.dirname(__file__))


def create_test_system():
    """Create a system we can use for testing."""
    xyz, size = generate_lattice('fcc', [3, 3, 3], density=0.9)
    low, high = [], []
    for i in size:
        low.append(i[0])
        high.append(i[1])
    box = create_box(low=low, high=high)
    system = System(units='lj', box=box)
    system.particles = Particles(dim=3)
    for xyzi in xyz:
        system.add_particle(name='Ar', pos=xyzi, vel=np.zeros_like(xyzi))
    system.particles.vel[-1] = np.array([-1.0, 0.123, 1.0])
    return system


def create_path():
    """Setup a simple path for a test."""
    system = create_test_system()
    system.particles.name = ['X'] * system.particles.npart
    system.box = create_box(length=rand(3))
    path = Path(None)
    phasepoints = []
    for _ in range(10):
        pos = rand(*system.particles.pos.shape)
        vel = rand(*pos.shape)
        vpot = random()
        ekin = random()
        phasepoint = {'order': [pos[0][0], pos[1][0]], 'pos': pos,
                      'vel': vel, 'vpot': vpot, 'ekin': ekin}
        phasepoints.append(phasepoint)
        path.append(phasepoint)
    return system, phasepoints, path


def _add_path_data(path_data, phasepoint):
    """Add path data for storage."""
    if not path_data:
        step = 0
    else:
        step = path_data[-1][0] + 1
    new_data = [step, phasepoint['pos'][0], phasepoint['pos'][1],
                phasepoint['vel']]
    path_data.append(new_data)


def create_external_path(random_length=False):
    """Create an external path for testing."""
    path_data = []
    path = PathExt(None)
    phasepoint = {'pos': ('initial.g96', None), 'vel': False,
                  'order': [random(), None], 'vpot': None,
                  'ekin': None}
    path.append(phasepoint)
    _add_path_data(path_data, phasepoint)
    if random_length:
        length = randint(10, 100)
    else:
        length = 5
    for i in range(length, 0, -1):
        phasepoint = {'pos': ('trajB.trr', i), 'vel': True,
                      'order': [random(), None], 'vpot': None,
                      'ekin': None}
        path.append(phasepoint)
        _add_path_data(path_data, phasepoint)
    for i in range(0, length):
        phasepoint = {'pos': ('trajF.trr', i), 'vel': False,
                      'order': [random(), None], 'vpot': None,
                      'ekin': None}
        path.append(phasepoint)
        _add_path_data(path_data, phasepoint)
    return path, path_data


def generate_snaplines(path_writer, conf_writer, phasepoints, system, path):
    """Genereate snapshots using path and traj writers."""
    snapshots = [line for line in path_writer.generate_output(0, path)]
    length = len([_ for _ in conf_writer.generate_output(0, system)])
    for i, phasepoint in enumerate(phasepoints):
        system.particles.set_particle_state(phasepoint)
        snap = conf_writer.generate_output(i, system)
        snap2 = snapshots[1+i*length:1+(i+1)*length]
        for line1, line2 in zip(snap, snap2):
            yield line1, line2


class TrajTest(unittest.TestCase):
    """Test trajectory writing work as intended."""

    def test_adjust_coordinates(self):
        """Test that we can adjust coordinates."""
        # 1D
        coord = np.array([10, ])
        coord_ = adjust_coordinate(coord)
        self.assertTrue(np.allclose(coord_, [[10., 0., 0.]]))
        # 1 particle, 1D
        particles = Particles(dim=1)
        particles.add_particle(np.array([1.0]),
                               np.zeros(1),
                               np.zeros(1))
        pos = adjust_coordinate(particles.pos)
        self.assertTrue(np.allclose(pos, np.array([1.0, 0.0, 0.0])))
        # 1 particle, 2D
        particles = Particles(dim=2)
        particles.add_particle(np.array([1.0, 1.0]),
                               np.zeros(2),
                               np.zeros(2))
        pos = adjust_coordinate(particles.pos)
        self.assertTrue(np.allclose(pos, np.array([1.0, 1.0, 0.0])))
        # 1 particle, 3D
        particles = Particles(dim=3)
        particles.add_particle(np.array([1.0, 1.0, 1.0]),
                               np.zeros(3),
                               np.zeros(3))
        pos = adjust_coordinate(particles.pos)
        self.assertTrue(np.allclose(pos, np.array([1.0, 1.0, 1.0])))
        # 2 particles, 1D
        particles = Particles(dim=1)
        particles.add_particle(np.array([1.0]),
                               np.zeros(1),
                               np.zeros(1))
        particles.add_particle(np.array([-1.0]),
                               np.zeros(1),
                               np.zeros(1))
        pos = adjust_coordinate(particles.pos)
        self.assertTrue(np.allclose(pos, np.array([[1., 0., 0.],
                                                   [-1., 0., 0.]])))
        # 2 particles, 2D
        particles = Particles(dim=2)
        particles.add_particle(np.array([1.0, -1.0]),
                               np.zeros(2),
                               np.zeros(2))
        particles.add_particle(np.array([-1.0, 1.0]),
                               np.zeros(2),
                               np.zeros(2))
        pos = adjust_coordinate(particles.pos)
        self.assertTrue(np.allclose(pos, np.array([[1., -1., 0.],
                                                   [-1., 1., 0.]])))

        # 3 particles, 3D
        particles = Particles(dim=3)
        particles.add_particle(np.array([1.0, -1.0, 0.5]),
                               np.zeros(3),
                               np.zeros(3))
        particles.add_particle(np.array([-1.0, 1.0, -0.5]),
                               np.zeros(3),
                               np.zeros(3))
        pos = adjust_coordinate(particles.pos)
        self.assertTrue(np.allclose(pos, np.array([[1., -1., 0.5],
                                                   [-1., 1., -0.5]])))

    def test_txt_writer(self):
        """Test the TrajWriter."""
        system = create_test_system()
        txt_writer = get_writer('trajtxt', {'fmt': 'yes'})
        snapshot = txt_writer.generate_output(0, system)
        correct = os.path.join(HERE, 'generated.txt')
        with open(correct, 'r') as fileh:
            for lines1, lines2 in zip(fileh, snapshot):
                self.assertEqual(lines1.rstrip(), lines2.rstrip())

    def test_traj_writer_novel(self):
        """Test the TrajWriter class when we include velocities."""
        writer = TrajWriter(write_vel=False, fmt='full')
        reader = TrajWriter()
        system, phasepoints, path = create_path()
        with tempfile.NamedTemporaryFile() as tmp:
            for step, snapshot in enumerate(path.trajectory()):
                system.particles.set_particle_state(snapshot)
                for line in writer.format_snapshot(step, system):
                    string = '{}\n'.format(line)
                    tmp.write(string.encode('utf-8'))
            tmp.flush()
            del writer
            for block, snapshot in zip(reader.load(tmp.name), phasepoints):
                self.assertTrue(np.allclose(block['box'], system.box.length))
                xyz = np.transpose(np.vstack((block['x'],
                                              block['y'],
                                              block['z'])))
                self.assertTrue(np.allclose(xyz, snapshot['pos']))

    def test_traj_writer_vel(self):
        """Test the TrajWriter class when we exclude velocities."""
        writer = TrajWriter(write_vel=True, fmt='full')
        reader = TrajWriter()
        system, phasepoints, path = create_path()
        with tempfile.NamedTemporaryFile() as tmp:
            for step, snapshot in enumerate(path.trajectory()):
                system.particles.set_particle_state(snapshot)
                for line in writer.format_snapshot(step, system):
                    string = '{}\n'.format(line)
                    tmp.write(string.encode('utf-8'))
            tmp.flush()
            del writer
            for block, snapshot in zip(reader.load(tmp.name), phasepoints):
                self.assertTrue(np.allclose(block['box'], system.box.length))
                xyz = np.transpose(np.vstack((block['x'],
                                              block['y'],
                                              block['z'])))
                vel = np.transpose(np.vstack((block['vx'],
                                              block['vy'],
                                              block['vz'])))
                self.assertTrue(np.allclose(xyz, snapshot['pos']))
                self.assertTrue(np.allclose(vel, snapshot['vel']))

    def test_path_int_writer(self):
        """Test the path internal writer."""
        _, phasepoints, path = create_path()
        writer = get_writer('pathtrajint')
        idxs = 0
        idx = 0
        for i, lines in enumerate(writer.generate_output(0, path, 'ACC')):
            if i == 0:
                self.assertEqual('# Cycle: 0, status: ACC', lines)
            else:
                if lines.startswith('Snapshot'):
                    self.assertEqual('Snapshot: {}'.format(idxs), lines)
                    idxs += 1
                    idx = 0
                else:
                    posvel = '{} {} {} {} {} {}'.format(
                        *phasepoints[idxs - 1]['pos'][idx],
                        *phasepoints[idxs - 1]['vel'][idx]
                    )
                    self.assertEqual(lines, posvel)
                    idx += 1

    def test_pathint_read_write(self):
        """Test that we can read write with PathIntWriter."""
        writer = PathIntWriter()
        reader = PathIntWriter()
        statuses = ('ACC', 'BWI', 'FTL')
        path_phasepoints = []
        with tempfile.NamedTemporaryFile() as tmp:
            for i, status in zip(range(3), statuses):
                _, phasepoints, path = create_path()
                path_phasepoints.append(phasepoints)
                for line in writer.generate_output(i, path, status):
                    tmp.write('{}\n'.format(line).encode('utf-8'))
            tmp.flush()
            del writer
            for status, path, phasepoints in zip(statuses,
                                                 reader.load(tmp.name),
                                                 path_phasepoints):
                self.assertEqual(status, path['comment'][0].split()[-1])
                for snapshot, phasepoint in zip(path['data'], phasepoints):
                    for key in ('pos', 'vel'):
                        self.assertTrue(np.allclose(snapshot[key],
                                                    phasepoint[key]))

    def test_pathint_read_error(self):
        """Test what happens in we read faulty input with PathIntWriter."""
        filename = os.path.join(HERE, 'traj-error1.txt')
        reader = PathIntWriter().load(filename)
        next(reader)
        next(reader)
        with self.assertRaises(ValueError):
            next(reader)
        filename = os.path.join(HERE, 'traj-error2.txt')
        reader = PathIntWriter().load(filename)
        with self.assertRaises(ValueError):
            next(reader)

    def test_path_ext_writer(self):
        """Test the path external writer."""
        path, _ = create_external_path()
        writer = get_writer('pathtrajext')
        correct = ['# Cycle: 0, status: ACC',
                   '#     Step              Filename       index    vel',
                   '         0           initial.g96           0      1',
                   '         1             trajB.trr           5     -1',
                   '         2             trajB.trr           4     -1',
                   '         3             trajB.trr           3     -1',
                   '         4             trajB.trr           2     -1',
                   '         5             trajB.trr           1     -1',
                   '         6             trajF.trr           0      1',
                   '         7             trajF.trr           1      1',
                   '         8             trajF.trr           2      1',
                   '         9             trajF.trr           3      1',
                   '        10             trajF.trr           4      1']

        for corr, snap in zip(correct, writer.generate_output(0, path, 'ACC')):
            self.assertEqual(corr, snap)

    def test_path_ext_read_write(self):
        """Test the read write for the PathExtWriter class."""
        writer = PathExtWriter()
        reader = PathExtWriter()
        statuses = ('ACC', 'BWI', 'FTL')
        all_path_data = []
        with tempfile.NamedTemporaryFile() as tmp:
            for i, status in zip(range(3), statuses):
                path, data = create_external_path(random_length=True)
                all_path_data.append(data)
                for line in writer.generate_output(i, path, status):
                    string = '{}\n'.format(line)
                    tmp.write(string.encode('utf-8'))
            tmp.flush()
            del writer
            for status, path, data in zip(statuses, reader.load(tmp.name),
                                          all_path_data):
                self.assertEqual(status, path['comment'][0].split()[-1])
                for snapshot, datai in zip(path['data'], data):
                    self.assertEqual(int(snapshot[0]), datai[0])
                    self.assertEqual(snapshot[1], datai[1])
                    if datai[2] is None:
                        self.assertEqual(int(snapshot[2]), 0)
                    else:
                        self.assertEqual(int(snapshot[2]), datai[2])
                    vel = True if snapshot[3] == '-1' else False
                    self.assertEqual(vel, datai[3])


if __name__ == '__main__':
    unittest.main()
